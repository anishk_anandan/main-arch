#!/bin/bash

# Maintenance script for Arch Linux systems, intended to be run
# weekly. Should be run as root to do cleanup tasks; if run as normal
# user will still print some useful information.

separator()
{
    echo "======================================================================"
}

sectionStart()
{
    echo
    separator
    echo
    echo "$1"
    echo

separator
echo -n "arch-maint report for `hostname` on `date`"

sectionStart "SYSTEM"
uname -a
echo
uptime

# Check systemd failed services
sectionStart "FAILED SERVICES"
systemctl --failed

# Log files check
#sectionStart"LOG FILES CHECK"
#sudo journalctl -p 3 -xb

# Update
sectionStart "PENDING UPDATES"
sudo pacman -Syu

# Yay Update
yay -Syu

#Delete Pacman Cache
sectionStart "CLEAR OLD PACKAGE CACHE"
sudo pacman -Scc

# Delete Yay Cache
yay -Scc

# Delete unwanted dependencies
sectionStart "UNWANTED DEPENDENCIES"
yay -Yc

# Check Orphan packages
sectionStart "CHECK ORPHANED PACKAGES"
pacman -Qtdq

sectionStart "REMOVE ORPHANED PACKAGES"
if [[ ! -n $(pacman -Qdt) ]]; then
    echo "No orphans to remove."
else
    pacman --noconfirm -Rns $(pacman -Qdtq)
fi

sectionStart "CLEAR OLD PACKAGE CACHE"
pacman  --noconfirm -Sc

sectionStart "OPTIMISE PACKAGE DATABASE"
pacman-optimize --nocolor

# Clean the Cache
sectionStart "CLEAN CACHE"
rm -rf .cache/*

# Clean the journal
sectionStart "CLEAN JOURNEL"
sudo journalctl --vacuum-time=2weeks

sectionStart "DISK SPACE AFTER CLEANUPS"
df -h
echo
du -hs /var

sectionStart "AUR PACKAGES"
pacman -Qem

sectionStart "EXPLICITLY INSTALLED PACKAGES"
pacman -Qen

separator
echo
echo "Report completed at `date`"
